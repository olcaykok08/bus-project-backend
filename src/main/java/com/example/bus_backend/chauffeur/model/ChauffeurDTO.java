package com.example.bus_backend.chauffeur.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChauffeurDTO implements Serializable {
    private String id;
    private String name;
    private String surName;
    private String email;
    private String phoneNumber;
}
