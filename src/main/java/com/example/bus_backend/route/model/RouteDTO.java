package com.example.bus_backend.route.model;

import com.example.bus_backend.station.model.StationDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RouteDTO implements Serializable {
    private String id;
    private String name;
    private List<StationDTO> station;
}
