package com.example.bus_backend.route.service;

import com.example.bus_backend.route.controller.RouteResource;
import com.example.bus_backend.route.model.RouteDTO;
import com.example.bus_backend.station.model.StationDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
class RouteServiceTest {
    @Autowired
    private  RouteService routeService;

    @Autowired
    private RouteResource routeResource;

    @Test
    void addRoute(){
        RouteDTO routeDTO = new RouteDTO();

        routeDTO.setName("Sağlık-hastane");
        routeDTO.setId("2");
        List<StationDTO> stationDTOList=new ArrayList<>();
        StationDTO stationDTO=new StationDTO();
        stationDTO.setId("4");
        stationDTO.setName("üniversite durağı");
        stationDTOList.add(stationDTO);

        routeDTO.setStation(stationDTOList);

        routeService.addRoute(routeDTO);
    }
}