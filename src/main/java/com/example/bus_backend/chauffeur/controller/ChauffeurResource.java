package com.example.bus_backend.chauffeur.controller;

import ch.qos.logback.core.pattern.util.RegularEscapeUtil;
import com.example.bus_backend.chauffeur.model.ChauffeurDTO;
import com.example.bus_backend.chauffeur.service.ChauffeurService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/private-management-api")
@AllArgsConstructor
public class ChauffeurResource {

    private ChauffeurService chauffeurService;

    @PostMapping("/chauffeur/save")
    public ResponseEntity<ChauffeurDTO>addChauffeur(@RequestBody ChauffeurDTO chauffeurDTO){
        return ResponseEntity.ok(chauffeurService.addChauffeur(chauffeurDTO));
    }

    @GetMapping("/chauffeur/delete/{id}")
    public ResponseEntity<Boolean>deleteChauffeur(@PathVariable String id){
        return ResponseEntity.ok(chauffeurService.deleteChauffeurById(id));
    }

    @GetMapping("/chauffeur/get/id/{id}")
    public ResponseEntity<ChauffeurDTO>findChauffeurById(@PathVariable String id){
        return ResponseEntity.ok(chauffeurService.findChauffeurById(id));
    }

    @GetMapping("/shauffeurs/get")
    public ResponseEntity<List<ChauffeurDTO>>findAllChauffeurs(){
        return ResponseEntity.ok(chauffeurService.getAllChauffeurs());
    }

}
