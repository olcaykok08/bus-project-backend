package com.example.bus_backend.bus.model;
import com.example.bus_backend.route.model.RouteDTO;
import com.example.bus_backend.station.model.StationDTO;
import lombok.Data;
import java.io.Serializable;
@Data
public class BusDTO implements Serializable {
    private String id;
    private String number;
    private RouteDTO route;
}