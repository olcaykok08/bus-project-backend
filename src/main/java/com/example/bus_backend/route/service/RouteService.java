package com.example.bus_backend.route.service;

import com.example.bus_backend.route.model.Route;
import com.example.bus_backend.route.model.RouteDTO;
import com.example.bus_backend.route.model.RouteMapper;
import com.example.bus_backend.route.repository.RouteRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RouteService {
    private final RouteRepository routeRepository;

    public RouteService(RouteRepository routeRepository) {
        this.routeRepository = routeRepository;
    }

    public RouteDTO addRoute(RouteDTO routeDTO){
        Route route = routeRepository.save(RouteMapper.toEntity(routeDTO));
        return RouteMapper.toDTO(route);
    }

    public  RouteDTO findRouteById(String id){
        Optional<Route> optional = routeRepository.findById(id);
        if(optional.isPresent()){
            Route route = optional.get();
            return RouteMapper.toDTO(route);
        }
        return null;
    }

    public Boolean deleteRouteById(String id){
        routeRepository.deleteById(id);
        if(findRouteById(id)==null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<RouteDTO> getAllRoutes(){

        List<RouteDTO> routeDTOS = new ArrayList<>();
        List<Route> routes = routeRepository.findAll();
        for (Route route:routes){
            routeDTOS.add(RouteMapper.toDTO(route));
        }
        return routeDTOS;
    }

    public Route findById(String id){
        Route route =routeRepository.findById(id).get();
        return route == null ? null : route;
    }


}
