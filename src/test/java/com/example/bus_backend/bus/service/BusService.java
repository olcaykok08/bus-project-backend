package com.example.bus_backend.bus.service;

import com.example.bus_backend.bus.controller.BusResource;
import com.example.bus_backend.bus.model.Bus;
import com.example.bus_backend.bus.model.BusDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
class BusServiceTest {
    @Autowired
    private BusService busService;

    @Autowired
    private BusResource busResource;

    @Test
    void addBus(){
       BusDTO busDTO =new BusDTO();
       busDTO.setId("4");
       busDTO.setNumber("4A");
       busService.addBus(busDTO);
    }
    @Test
    void deleteBusById() {
        busService.deleteBusById("4");

    }
    @Test
    void getAllBus(){
        System.out.println(busService.getAllBuses());
    }

    @Test
    void findById() {
        System.out.println(busService.findById("4"));
    }
}
