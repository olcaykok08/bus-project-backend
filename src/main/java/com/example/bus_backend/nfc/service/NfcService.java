package com.example.bus_backend.nfc.service;

import com.example.bus_backend.nfc.model.Nfc;
import com.example.bus_backend.nfc.model.NfcDTO;
import com.example.bus_backend.nfc.model.NfcMapper;
import com.example.bus_backend.nfc.repository.NfcRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NfcService {

    private final NfcRepository nfcRepository;

    public NfcService(NfcRepository nfcRepository) {
        this.nfcRepository = nfcRepository;
    }

    public NfcDTO addNfc(NfcDTO nfcDTO){
        Nfc nfc = nfcRepository.save(NfcMapper.toEntity(nfcDTO));
        return NfcMapper.toDTO(nfc);
    }

    public NfcDTO findNfcById(String id){
        Optional<Nfc> optional = nfcRepository.findById(id);
        if(optional.isPresent()){
            Nfc nfc = optional.get();
            return NfcMapper.toDTO(nfc);
        }
        return null;
    }

    public Boolean deleteNfcById(String id){
        nfcRepository.deleteById(id);
        if(findNfcById(id)==null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<NfcDTO> getAllNfcs(){
        List<NfcDTO> nfcDTOS =new ArrayList<>();
        List<Nfc> nfcs = nfcRepository.findAll();
        for (Nfc nfc: nfcs){
            nfcDTOS.add(NfcMapper.toDTO(nfc));
        }
        return nfcDTOS;
    }

    public Nfc findById(String id){
        Nfc nfc = nfcRepository.findById(id).get();
        return nfc == null ? null : nfc;
    }

}
