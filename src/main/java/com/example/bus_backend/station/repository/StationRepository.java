package com.example.bus_backend.station.repository;

import com.example.bus_backend.station.model.Station;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRepository extends MongoRepository<Station,String> {
}
