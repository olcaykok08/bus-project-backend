package com.example.bus_backend.nfc.model;

import com.example.bus_backend.route.model.RouteDTO;
import com.example.bus_backend.station.model.Station;
import com.example.bus_backend.station.model.StationDTO;
import lombok.*;
import java.io.Serializable;

@Data
public class NfcDTO  implements Serializable {
    private String id;
    private String number;
}
