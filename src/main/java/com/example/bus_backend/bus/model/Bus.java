package com.example.bus_backend.bus.model;


import com.example.bus_backend.route.model.Route;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Data
@Document
public class Bus implements Serializable {
    @Id
    private String id;

    @Field
    private String number;

    @DBRef
    @Field
    private Route route;

}