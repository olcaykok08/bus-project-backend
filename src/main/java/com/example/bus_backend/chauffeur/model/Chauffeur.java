package com.example.bus_backend.chauffeur.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
@Data
@Document
public class Chauffeur implements Serializable {
    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String surName;
    @Field
    private String email;
    @Field
    private String phoneNumber;
}
