package com.example.bus_backend.bus.model;

import com.example.bus_backend.route.model.RouteMapper;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class BusMapper implements Serializable {
    public static BusDTO toDTO(Bus bus){
        if(bus==null){
            return null;
        }
        BusDTO busDTO = new BusDTO();
        busDTO.setId(bus.getId());
        busDTO.setNumber((bus.getNumber()));
        busDTO.setRoute(RouteMapper.toDTO(bus.getRoute()));
        return busDTO;
    }
    public static Bus toEntity(BusDTO busDTO){
        if(busDTO==null){
            return null;
        }
        Bus bus = new Bus();

        bus.setId(busDTO.getId());
        bus.setNumber(busDTO.getNumber());
        bus.setRoute(RouteMapper.toEntity(busDTO.getRoute()));
        return bus;
    }
}