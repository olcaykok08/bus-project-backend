package com.example.bus_backend.route.model;

import com.example.bus_backend.station.model.Station;
import com.example.bus_backend.station.model.StationDTO;
import com.example.bus_backend.station.model.StationMapper;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
public class RouteMapper implements Serializable {
    public static RouteDTO toDTO(Route route){
        if(route==null){
            return null;
        }
        RouteDTO routeDTO=new RouteDTO();

        routeDTO.setId(route.getId());
        routeDTO.setName(route.getName());

        List<StationDTO> stationDTOList=StationMapper.listToDTO(route.getStation());
        routeDTO.setStation(stationDTOList);
        return routeDTO;
    }
    public static Route toEntity(RouteDTO routeDTO){
        if(routeDTO==null){
            return null;
        }
        Route route= new Route();
        route.setId(routeDTO.getId());
        route.setName(routeDTO.getName());

        List<Station> stationList = StationMapper.listToEntity(routeDTO.getStation());
        route.setStation((stationList));
        return route;
    }
}
