package com.example.bus_backend.station.controller;

import com.example.bus_backend.station.model.Station;
import com.example.bus_backend.station.model.StationDTO;
import com.example.bus_backend.station.model.StationMapper;
import com.example.bus_backend.station.service.StationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StationResource {
    private final StationService stationService;

    public StationResource(StationService stationService){
        this.stationService=stationService;
    }

    @PostMapping("/station/save")
    public ResponseEntity<StationDTO> addStation(@RequestBody StationDTO stationDTO){
        return ResponseEntity.ok(stationService.addStation(stationDTO));
    }

    @GetMapping("/station/delete/{id}")
    public ResponseEntity<Boolean> deleteStationById(@RequestBody String id){
        return ResponseEntity.ok(stationService.deleteStationById(id));
    }

    @GetMapping("/station/get/id/{id}")
    public ResponseEntity<StationDTO>findStationById(@PathVariable String id){
        return ResponseEntity.ok(stationService.findStationById(id));
    }

    @GetMapping("/stations/get")
    public ResponseEntity<List<StationDTO>> getAllStations(){
        return  ResponseEntity.ok(stationService.getAllStations());
    }
    @GetMapping("/station/get/{id}")
    public ResponseEntity<StationDTO> getStationById(@PathVariable String id){
        Station station = stationService.findById(id);
        return station == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(StationMapper.toDTO(station));
    }
}

