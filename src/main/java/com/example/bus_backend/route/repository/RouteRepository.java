package com.example.bus_backend.route.repository;

import com.example.bus_backend.route.model.Route;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends MongoRepository<Route,String> {
}
