package com.example.bus_backend.nfc.controller;

import com.example.bus_backend.nfc.model.Nfc;
import com.example.bus_backend.nfc.model.NfcDTO;
import com.example.bus_backend.nfc.model.NfcMapper;
import com.example.bus_backend.nfc.service.NfcService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NfcResource {
    private final NfcService nfcService;

    public NfcResource(NfcService nfcService){this.nfcService=nfcService;}

    @PostMapping("/nfc/save")
    public ResponseEntity<NfcDTO> addNfc(@RequestBody NfcDTO nfcDTO){
        return  ResponseEntity.ok(nfcService.addNfc(nfcDTO));
    }

    @GetMapping("/nfc/delete/{id}")

    public ResponseEntity<Boolean>deleteNfcById(@RequestBody String id){
        return ResponseEntity.ok(nfcService.deleteNfcById(id));
    }

    @GetMapping("nfc/get/id/{id}")
    public ResponseEntity<NfcDTO>findNfcById(@RequestBody String id){
        return ResponseEntity.ok(nfcService.findNfcById(id));
    }

    @GetMapping("/nfcs/get")
    public ResponseEntity<List<NfcDTO>> getAllNfcs(){
        return ResponseEntity.ok(nfcService.getAllNfcs());
    }

    @GetMapping("/nfc/get/{id}")
    public ResponseEntity<NfcDTO> getNfcById(@PathVariable String id){
        Nfc nfc = nfcService.findById(id);
        return nfc == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(NfcMapper.toDTO(nfc));
    }
}
