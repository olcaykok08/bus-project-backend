package com.example.bus_backend.nfc.service;

import com.example.bus_backend.bus.controller.BusResource;
import com.example.bus_backend.nfc.controller.NfcResource;
import com.example.bus_backend.nfc.model.NfcDTO;
import com.example.bus_backend.route.model.Route;
import com.example.bus_backend.route.model.RouteDTO;
import com.example.bus_backend.station.model.StationDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.mapping.DBRef;

@SpringBootTest
@AutoConfigureMockMvc
class NfcServiceTest {
    @Autowired
    private NfcService nfcService ;

    @Autowired
    private NfcResource nfcResource;

    @Test
    void addNfc(){
        NfcDTO nfcDTO = new NfcDTO();

        nfcDTO.setId("4");
        nfcDTO.setNumber("4");
        nfcService.addNfc(nfcDTO);
    }
}
