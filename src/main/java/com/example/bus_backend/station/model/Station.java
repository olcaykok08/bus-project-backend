package com.example.bus_backend.station.model;

import com.example.bus_backend.bus.model.Bus;
import com.example.bus_backend.nfc.model.Nfc;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
@Data
@Document
public class Station {
    @Id
    private String id;

    @Field
    private String name;

    @Field
    private String passengerNumber;

    @DBRef
    @Field
    private Nfc nfc;

}
