package com.example.bus_backend.station.model;

import com.example.bus_backend.bus.model.BusDTO;
import com.example.bus_backend.nfc.model.NfcDTO;
import lombok.*;

@Data
public class StationDTO {
    private String id;
    private String name;
    private String passengerNumber;
    private NfcDTO nfc;


}
