package com.example.bus_backend.chauffeur.service;

import com.example.bus_backend.chauffeur.model.Chauffeur;
import com.example.bus_backend.chauffeur.model.ChauffeurDTO;
import com.example.bus_backend.chauffeur.model.ChauffeurMapper;
import com.example.bus_backend.chauffeur.repository.ChauffeurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class ChauffeurService {

    private ChauffeurRepository chauffeurRepository;

    public ChauffeurDTO addChauffeur(ChauffeurDTO chauffeurDTO){
        Chauffeur chauffeur = chauffeurRepository.save(ChauffeurMapper.toCollection(chauffeurDTO));
        log.info(String.valueOf(chauffeur));
        return ChauffeurMapper.toDTO(chauffeur);
    }

    public ChauffeurDTO findChauffeurById(String id) {
        Optional<Chauffeur> optional = chauffeurRepository.findById(id);
        if (optional.isPresent()){
            Chauffeur chauffeur = optional.get();
            return ChauffeurMapper.toDTO(chauffeur);
        }
        return null;
    }
    public Boolean deleteChauffeurById(String id){
        chauffeurRepository.deleteById(id);
        if (findChauffeurById(id) == null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<ChauffeurDTO> getAllChauffeurs(){
        List<ChauffeurDTO> chauffeurDTOS = new ArrayList<>();
        List<Chauffeur> chauffeurs = chauffeurRepository.findAll();

        for (Chauffeur chauffeur : chauffeurs){
            chauffeurDTOS.add(ChauffeurMapper.toDTO(chauffeur));
        }
        return chauffeurDTOS;
    }

}
