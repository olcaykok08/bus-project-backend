package com.example.bus_backend.nfc.model;

import com.example.bus_backend.route.model.Route;
import com.example.bus_backend.station.model.Station;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
@Data
@Document
public class Nfc implements Serializable {
    @Id
    private String id;

    @Field
    private String number;

}
