package com.example.bus_backend;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@EnableWebSecurity
@AllArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    //private AuthenticationService authenticationService;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                //username and psw don't need any authentication (we provide this with OPTIONS)
                .antMatchers(HttpMethod.OPTIONS,"/app/**").permitAll()
                // but other requests need authentication
                .antMatchers("/private/app-api/**").permitAll()
                .antMatchers("/public-app-api/**").authenticated()

                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
/*
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationService);
    }

 */
}
