package com.example.bus_backend.route.controller;

import com.example.bus_backend.route.model.Route;
import com.example.bus_backend.route.model.RouteDTO;
import com.example.bus_backend.route.model.RouteMapper;
import com.example.bus_backend.route.service.RouteService;
import com.example.bus_backend.station.model.StationDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ResourceBundle;

@RestController
public class RouteResource {
    private final RouteService routeService;

    public RouteResource(RouteService routeService){
        this.routeService=routeService;
    }

    @PostMapping("/route/save")
    public ResponseEntity<RouteDTO> addRoute(@RequestBody RouteDTO routeDTO){
        return ResponseEntity.ok(routeService.addRoute(routeDTO));
    }

    @GetMapping("/route/delete/{id}")

    public ResponseEntity<Boolean>deleteRouteById(@RequestBody String id){
        return ResponseEntity.ok(routeService.deleteRouteById(id));
    }

    @GetMapping("/route/get/id/{id}")

    public ResponseEntity<RouteDTO>findRouteById(@RequestBody String id){
        return ResponseEntity.ok(routeService.findRouteById(id));
    }

    @GetMapping("/routes/get")

    public ResponseEntity<List<RouteDTO>> getAllRoutes(){
        return ResponseEntity.ok(routeService.getAllRoutes());
    }

    @GetMapping("/route/get/{id}")
    public ResponseEntity<RouteDTO> getRouteById(@PathVariable String id){
        Route route = routeService.findById(id);
        return route == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(RouteMapper.toDTO(route));
    }
}
