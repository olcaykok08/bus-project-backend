package com.example.bus_backend.chauffeur.model;

import java.io.Serializable;

public class ChauffeurMapper implements Serializable {

    public static ChauffeurDTO toDTO(Chauffeur chauffeur){
        if (chauffeur == null) {
            return null;
        }
        ChauffeurDTO chauffeurDTO = new ChauffeurDTO();
        chauffeurDTO.setId(chauffeur.getId());
        chauffeurDTO.setName(chauffeur.getName());
        chauffeurDTO.setSurName(chauffeur.getSurName());
        chauffeurDTO.setEmail(chauffeur.getEmail());
        chauffeurDTO.setPhoneNumber(chauffeur.getPhoneNumber());

        return chauffeurDTO;
    }

    public static Chauffeur toCollection(ChauffeurDTO chauffeurDTO){
        if (chauffeurDTO == null) {
            return null;
        }

        Chauffeur chauffeur = new Chauffeur();
        chauffeur.setId(chauffeurDTO.getId());
        chauffeur.setName(chauffeurDTO.getName());
        chauffeur.setSurName(chauffeurDTO.getSurName());
        chauffeur.setEmail(chauffeurDTO.getEmail());
        chauffeur.setPhoneNumber(chauffeurDTO.getPhoneNumber());

        return chauffeur;
    }
}
