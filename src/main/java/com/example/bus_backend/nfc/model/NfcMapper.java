package com.example.bus_backend.nfc.model;

import com.example.bus_backend.route.model.RouteMapper;
import com.example.bus_backend.station.model.StationMapper;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class NfcMapper implements Serializable {
    public static NfcDTO toDTO(Nfc nfc) {
        if (nfc == null) {
            return null;
        }

        NfcDTO nfcDTO = new NfcDTO();

        nfcDTO.setId(nfc.getId());
        nfcDTO.setNumber(nfc.getNumber());
        return nfcDTO;
    }
    public static Nfc toEntity(NfcDTO nfcDTO) {
        if (nfcDTO == null) {
            return null;
        }
        Nfc nfc = new Nfc();

        nfc.setId(nfcDTO.getId());
        nfc.setNumber(nfcDTO.getNumber());
        return nfc;
    }
}