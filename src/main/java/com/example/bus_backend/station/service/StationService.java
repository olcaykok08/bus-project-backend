package com.example.bus_backend.station.service;

import com.example.bus_backend.station.model.Station;
import com.example.bus_backend.station.model.StationDTO;
import com.example.bus_backend.station.model.StationMapper;
import com.example.bus_backend.station.repository.StationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StationService {
    private final StationRepository stationRepository;

    public StationService(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    public StationDTO addStation(StationDTO stationDTO){
        Station station = stationRepository.save(StationMapper.toEntity(stationDTO));
        return StationMapper.toDTO(station);
    }

    public StationDTO findStationById(String id){
        Optional<Station> optional = stationRepository.findById(id);
        if (optional.isPresent()) {
            Station station = optional.get();
            return StationMapper.toDTO(station);
        }
        return null;
    }

    public Boolean deleteStationById(String id){
        stationRepository.deleteById(id);
        if (findStationById(id) == null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<StationDTO> getAllStations() {
        List<StationDTO> stationDTOS = new ArrayList<>();
        List<Station> stations = stationRepository.findAll();
        for ( Station station: stations){
            stationDTOS.add(StationMapper.toDTO(station));
        }
        return stationDTOS;
    }

    public Station findById(String id){
        Station station = stationRepository.findById(id).get();
        return station == null ? null : station;
    }
}
