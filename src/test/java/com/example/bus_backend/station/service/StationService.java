package com.example.bus_backend.station.service;

import com.example.bus_backend.bus.model.BusDTO;
import com.example.bus_backend.nfc.model.NfcDTO;
import com.example.bus_backend.station.controller.StationResource;
import com.example.bus_backend.station.model.StationDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc

class StationServiceTest {
    @Autowired
    private StationService stationService;

    @Autowired
    private StationResource stationResource;

    @Test
    void addStation(){
        StationDTO stationDTO = new StationDTO();
        NfcDTO nfcDTO= new NfcDTO();

        stationDTO.setPassengerNumber("16");
        stationDTO.setName("Kemal Durağı");
        stationDTO.setId("1");
        stationDTO.setNfc(nfcDTO);
        nfcDTO.setId("1");
        stationService.addStation(stationDTO);
    }
}
