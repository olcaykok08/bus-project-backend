package com.example.bus_backend.nfc.repository;

import com.example.bus_backend.nfc.model.Nfc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NfcRepository extends MongoRepository<Nfc,String> {

}
