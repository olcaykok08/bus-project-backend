package com.example.bus_backend.bus.repositroy;

import com.example.bus_backend.bus.model.Bus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusRepository extends MongoRepository<Bus,String>{
}
