package com.example.bus_backend.bus.service;

import com.example.bus_backend.bus.model.Bus;
import com.example.bus_backend.bus.model.BusDTO;
import com.example.bus_backend.bus.model.BusMapper;
import com.example.bus_backend.bus.repositroy.BusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BusService {
    private final BusRepository busRepository;

    public BusDTO addBus(BusDTO busDTO){
        Bus bus = busRepository.save(BusMapper.toEntity(busDTO));
        return BusMapper.toDTO(bus);
    }
    public BusDTO findBusById(String id){
        Optional<Bus> optional =busRepository.findById(id);
        if(optional.isPresent()){
            Bus bus =optional.get();
            return BusMapper.toDTO(bus);
        }
        return null;
    }

    public Boolean deleteBusById(String id){
        busRepository.deleteById(id);
        if(findBusById(id)==null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<BusDTO> getAllBuses(){
        List<BusDTO> busDTOS = new ArrayList<>();
        List<Bus> buses = busRepository.findAll();
        for (Bus bus:buses){
            busDTOS.add(BusMapper.toDTO(bus));
        }
        return busDTOS;
    }

    public Bus findById(String id){
        Optional<Bus> bus = busRepository.findById(id);
        return bus == null ? null:bus.get();
    }
}
