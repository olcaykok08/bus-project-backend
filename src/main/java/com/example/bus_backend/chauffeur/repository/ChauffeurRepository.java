package com.example.bus_backend.chauffeur.repository;

import com.example.bus_backend.chauffeur.model.Chauffeur;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChauffeurRepository extends MongoRepository<Chauffeur, String> {
}
