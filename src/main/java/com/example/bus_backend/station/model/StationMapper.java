package com.example.bus_backend.station.model;

import com.example.bus_backend.bus.model.BusMapper;
import com.example.bus_backend.nfc.model.NfcMapper;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
public class StationMapper implements Serializable {
    public static StationDTO toDTO(Station station){
        if(station==null){
            return null;
        }

        StationDTO stationDTO=new StationDTO();

        stationDTO.setId(station.getId());
        stationDTO.setName(station.getName());
        stationDTO.setPassengerNumber(station.getPassengerNumber());
        stationDTO.setNfc(NfcMapper.toDTO((station.getNfc())));

        return stationDTO;
    }
    public static Station toEntity(StationDTO stationDTO){
        if(stationDTO==null){
            return null;
        }
        Station station=new Station();
        station.setId(stationDTO.getId());
        station.setName(stationDTO.getName());
        station.setPassengerNumber(stationDTO.getPassengerNumber());
        station.setNfc(NfcMapper.toEntity(stationDTO.getNfc()));

        return station;
    }

    public static List<Station> listToEntity(List<StationDTO> stationDTOList){

        if(stationDTOList.isEmpty()){
            return null;

        }

        List<Station> stationList=new ArrayList<>();

        stationDTOList.stream().forEach(stationDTO -> {
            Station station=StationMapper.toEntity(stationDTO);
            stationList.add(station);

        } );

        return stationList;
    }

    public static List<StationDTO> listToDTO(List<Station> stationList){

        if(stationList.isEmpty()){
            return null;

        }

        List<StationDTO> stationDTOList=new ArrayList<>();

        stationList.stream().forEach(station -> {
            StationDTO stationDTO=StationMapper.toDTO(station);
            stationDTOList.add(stationDTO);

        } );

        return stationDTOList;
    }
}
