package com.example.bus_backend.bus.controller;

import com.example.bus_backend.bus.model.Bus;
import com.example.bus_backend.bus.model.BusDTO;
import com.example.bus_backend.bus.model.BusMapper;
import com.example.bus_backend.bus.service.BusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BusResource {
    private final BusService busService;

    public BusResource(BusService busService) {
        this.busService = busService;
    }

    @PostMapping("/bus/save")
    public ResponseEntity<BusDTO> addBus(@RequestBody BusDTO busDTO) {
        return ResponseEntity.ok(busService.addBus(busDTO));
    }

    @GetMapping("/bus/delete/{id}")
    public ResponseEntity<Boolean> deleteBusById(@RequestBody String id){
        return ResponseEntity.ok(busService.deleteBusById(id));
    }

   @GetMapping("/bus/get/id/{id}")
    public ResponseEntity<BusDTO>findBusById(@PathVariable String id){
        return ResponseEntity.ok(busService.findBusById(id));
   }

   @GetMapping("/buses/get")
    public ResponseEntity<List<BusDTO>> getAllBuses(){
        return ResponseEntity.ok(busService.getAllBuses());
   }

   @GetMapping("/bus/get/{id}")
    public ResponseEntity<BusDTO> getBusById(@PathVariable String id){
        Bus bus = busService.findById(id);
        return bus == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(BusMapper.toDTO(bus));
   }
}